**Реализуйте главный экран приложения «Книги» (только клиентскую часть)**

![главный экран](https://www.dropbox.com/s/hoi9s7cfqwt8ozv/preview_1.jpg?raw=1)

Сайт необходимо разработать без использования framework Bootstrap и ему подобных. Допускается использование небольших библиотек, решающих конкретную задачу.  

Должна быть поддержана адаптивность (максимальные и минимальные размеры брать из макета) и кроссбраузерность (последние версии Chrome, Firefox).

### Для разработки приложения использовать следующие технологии:  

- SCSS
- JavaScript ES6+
- Webpack
- Реализовать приложение на React.


При выборе фильтра (All Books, Most Recent, Most Popular, Free Books) страница не должна перезагружаться или осуществлять переход на другую страницу.

При уменьшении страницы по горизонтали не должна быть видна полоса прокрутки по горизонтали, контент должен подстраиваться под размер окна.

### На сайте должно быть реализовано добавление новой книги:

1. Открытие модального окна
2. Переход по вкладкам в модальном окне должно выполняться без перезагрузки
3. Реализовать валидацию формы:
    - поля Title, Author и ISBN не должны быть пустыми
    - поле ISBN может содержать только цифры
    - если форма не прошла валидацию, то необходимо вывести ошибки в этом же модальном окне
4. Если валидация пройдена, то необходимо вывести сообщение об успешном добавлении книги с указанием ее названия (то что было введено в поле Title)
5. Книга должна сохраняться в LocalStorage и отображаться в списке книг

### Просмотр книги
- Для просмотра книги использовать модальное окно добавления книги.
- Все inputs должны выглядеть как текст.

#### Работа с книгами
- Сохранять список книг в LocalStorage, а сам список книг считывать из LocalStorage (в этом случае лучше заранее оптимизировать картинки по размеру и качеству, для уменьшения их «веса»).

### Будет плюсом
- Реализовать блок с последними действиями в sidebar, как он изображен. Выводить последние 3 действия.

____

Результат разместите на GitLab или GitHub.
